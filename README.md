# Log Classification #

###  This project contains everything needed to parse and classify logs into predefined categories. Each model contains the notebook used for training along with all the data. ###

## Description ##

Detailed comments are added in each file. Log Classification contains a parser for the logs that
[LogDissect](https://github.com/dogoncouch/logdissect) could not parse, a type_setter that handles adding a type to each line of a log based on key words, and a log log classifier that trains on 80 percent of the data and tests on the remaining 20 percent. The log classifier provides a performance report and saces the model. Model 4 and 5 limit the amount of lines that the algorithm is training on in order to get more diversity in without crashing google colab.



### Usage ##

* Parse data
* Create type file for each log
* Train model
* Save model

### Dependencies ###

- [Pytorch](https://pytorch.org/)

-  [Scikit Learn](https://scikit-learn.org/stable/install.html)

- [Pandas](https://pandas.pydata.org/)

- [NumPy](https://numpy.org/install/)


### Contact? ###

* [Email](skylaurman@gmail.com)
* [LinkedIn](https://www.linkedin.com/in/skylaanne/)